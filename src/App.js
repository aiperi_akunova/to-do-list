import './App.css';
import AddTaskForm from "./AddTaskForm/AddTaskForm";
import Task from "./Tasks/Task";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {add, deleteTask, fetchTask, saveTask} from "./store/actions";
import Counter from "./container/Counter/Counter";

const App = () => {
    const [inputValue, setInputValue] = useState('')
    const  dispatch = useDispatch();
    const allTasks = useSelector(state=> state.tasks);
    console.log('allTasks', allTasks);

    useEffect(()=>{
        dispatch(fetchTask())
    },[dispatch])

    const addTask = (e)=>{
        e.preventDefault();
        // dispatch(add({text: inputValue, id: Math.random()}));
        dispatch(saveTask({text: inputValue, id: Math.random()}))
        setInputValue('')
    }

    const inputOnChange = (value)=>{
       setInputValue(value);
    }

    const taskComponents = Object.keys(allTasks).map(task =>(
        <Task
            key={allTasks[task].id}
            taskMessage = {allTasks[task].text}
            onDelete = {()=>dispatch(deleteTask(allTasks[task].id))}>
        </Task>
        ))

  return (
    <div className="App">
        <AddTaskForm
            onAdd = {addTask}
            value = {inputValue}
        inputChange={(e)=>inputOnChange(e.target.value)}/>
        {taskComponents}

        <div>
            <Counter/>
        </div>
    </div>
  );
};

export default App;
