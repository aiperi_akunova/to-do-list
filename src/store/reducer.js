import {ADD_TASK, DELETE_TASK, FETCH_TASK_FAILURE, FETCH_TASK_REQUEST, FETCH_TASK_SUCCESS} from "./actions";

const initialState = {
    tasks: [],
    loading: false,
    error: null
};
const reducer = (state = initialState, action) => {

    switch (action.type){
        case ADD_TASK:
           return  {...state, tasks: state.tasks.push(action.payload)};
        case DELETE_TASK:
            return {...state, tasks: state.tasks.filter(task=>task.id!== action.payload)};
        case FETCH_TASK_REQUEST:
            return {...state, error: null, loading: true};
        case FETCH_TASK_SUCCESS:
            return {...state, loading: false, tasks: action.payload};
        case FETCH_TASK_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return  state;
    }
}
export default reducer;