import axios from "axios";

export const ADD_TASK = 'ADD_TASK';
export const DELETE_TASK = 'DELETE_TASK';

export const add = (value)=> ({type: ADD_TASK, payload: value});
export const deleteTask = (taskId)=>({type: DELETE_TASK, payload: taskId})

export const FETCH_TASK_REQUEST='FETCH_TASK_REQUEST';
export const FETCH_TASK_SUCCESS='FETCH_TASK_SUCCESS';
export const FETCH_TASK_FAILURE='FETCH_TASK_FAILURE';

export const fetch_task_request = ()=>({type: FETCH_TASK_REQUEST });
export const fetch_task_success = counter => ({type: FETCH_TASK_SUCCESS, payload:counter });
export const fetch_task_failure = ()=>({type: FETCH_TASK_FAILURE});

export const fetchTask =()=>{
    return async (dispatch, getState)=>{
        dispatch(fetch_task_request());
        try{
            const response = await axios.get('https://blog-93444-default-rtdb.firebaseio.com/tasks.json');

            if(response.data === null){
                // dispatch(fetch_counter_success({taskText: 'Do homework', id: 1}))
            }else{
                dispatch(fetch_task_success(response.data));
                console.log('response', response.data)
            }
        }catch (e){
            dispatch(fetch_task_failure())
        }
    }
}

export const saveTask = (value)=>{
    return async (dispatch, getState)=>{
        await axios.post('https://blog-93444-default-rtdb.firebaseio.com/tasks.json',value);
    }
}
