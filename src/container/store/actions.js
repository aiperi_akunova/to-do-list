import axios from "axios";

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST='FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS='FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE='FETCH_COUNTER_FAILURE';

export const increase = ()=>({type: INCREASE});
export const decrease = ()=>({type: DECREASE});
export const add = value =>({type: ADD, payload: value});
export const subtract = value =>({type: SUBTRACT, payload: value});

export const fetch_counter_request = ()=>({type: FETCH_COUNTER_REQUEST });
export const fetch_counter_success = counter => ({type: FETCH_COUNTER_SUCCESS, payload:counter });
export const fetch_counter_failure = ()=>({type: FETCH_COUNTER_FAILURE});

export const fetchCounter =()=>{
    return async (dispatch, getState)=>{
        dispatch(fetch_counter_request());
        try{
            const response = await axios.get('https://blog-93444-default-rtdb.firebaseio.com/counter.json');
            console.log('response', response.data)

            if(response.data === null){
                dispatch(fetch_counter_success(0))
            }else{
                dispatch(fetch_counter_success(response.data));
            }
        }catch (e){
            dispatch(fetch_counter_failure())
        }
    }
}

export const saveCounter = ()=>{
    return async (dispatch, getState)=>{
        const currentState = getState().counter;
        await axios.put('https://blog-93444-default-rtdb.firebaseio.com/counter.json',currentState);
    }
}

/*
case FETCH_COUNTER_SUCCESS:
    return {...state, loading:false,counter: action.payload}
 */