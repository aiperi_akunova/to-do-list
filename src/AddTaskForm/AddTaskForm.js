import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = props => {
    return (
        <div className='form-block' >
            <form className='task-form' onSubmit={props.onAdd}>
                <input type='text' placeholder='Add new task' className='task-input' onChange={props.inputChange} value={props.value}/>
                <button type={'submit'} className='add-btn'>Add</button>
            </form>
        </div>
    );
};

export default AddTaskForm;