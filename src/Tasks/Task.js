import React from 'react';
import './Task.css';

const Task = props => {
    return (
        <div>
            <div className="task">
                <p>{props.taskMessage}</p>
                <button className="delete-btn" onClick={props.onDelete}>Delete</button>
            </div>
        </div>
    );
};

export default Task;